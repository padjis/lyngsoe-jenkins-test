pipeline {
    agent any
    options { timestamps () }
    environment {
        NOW = new Date().format( 'yyyyMMddHHmm' )
    }
    stages {
        stage('Build') {
            steps {
                echo 'Building ...' + env.BRANCH_NAME
                sh './gradlew build'
            }
        }
    }
    post {
        changed {
            echo 'Notifying ...' + env.BRANCH_NAME
            notifyBuild(currentBuild.result)
            cleanWs()
        }
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  discordSend description: summary, footer: "Footer Text", link: env.BUILD_URL, result: currentBuild.currentResult, title: JOB_NAME, webhookURL: "https://discordapp.com/api/webhooks/1090890140307705897/4rkUt0i-8bFZcdXMY3zepjJtY-z_OEex1dj_BgN0e3RxcgwYBUX815gBVVcJXDKCzC9f"
}